call composer update
call drush site-install --site-name="Open clinic"
call drush en openclinic_core  -y
call drush en openclinic_install  -y 
call drush en openclinic_pricefield  -y
call drush en openclinic_views -y
call drush en rest -y
call composer require drupal/admin_toolbar
call drush en admin_toolbar  -y
@echo Open clinic installed
