INTRODUCTION
------------

Openclinic pricefield module is a module for creating price field.
This field may use for entities creating.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable the module.


CONFIGURATION
-------------

* Configure field upon entities type creating.


MAINTAINERS
-----------

Current maintainers:
 * Sergey Loginov (goodboy) - https://drupal.org/user/222910
