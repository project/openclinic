<?php

namespace Drupal\openclinic_pricefield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldFilteredMarkup;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'openclinic_price_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "openclinic_price_widget_type",
 *   module = "openclinic_pricefield",
 *   label = @Translation("Openclinic price widget type"),
 *   field_types = {
 *     "openclinic_price"
 *   }
 * )
 */
class OpenclinicPriceWidgetType extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : NULL;
    $field_settings = $this->getFieldSettings();

    $element['item'] = [
      '#type' => 'textfield',
      '#title' => t('Price item'),
      '#default_value' => isset($items[$delta]->item) ? $items[$delta]->item : NULL,
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => $this->getFieldSetting('max_length'),
    ];

    $element['number'] = [
      '#type' => 'textfield',
      '#title' => t('Price value'),
      '#default_value' => isset($items[$delta]->number) ? $items[$delta]->number : NULL,
      '#placeholder' => $this->getSetting('placeholder'),
      '#step' => pow(0.1, $this->getSetting('scale')),
    ];

    // Set minimum and maximum.
    if (is_numeric($field_settings['min'])) {
      $element['number']['#min'] = $field_settings['min'];
    }
    if (is_numeric($field_settings['max'])) {
      $element['number']['#max'] = $field_settings['max'];
    }

    // Add prefix and suffix.
    if ($field_settings['prefix']) {
      $prefixes = explode('|', $field_settings['prefix']);
      $element['number']['#field_prefix'] = FieldFilteredMarkup::create(array_pop($prefixes));
    }
    if ($field_settings['suffix']) {
      $suffixes = explode('|', $field_settings['suffix']);
      $element['number']['#field_suffix'] = FieldFilteredMarkup::create(array_pop($suffixes));
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return $element['numeric'];
  }

}
