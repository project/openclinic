INTRODUCTION
------------

Openclinic views module integrate Views module to Openclinic.
After the module enabled views will be created and deleted
together with related entities.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable the module and depending modules.


CONFIGURATION
-------------

* Go to [Your Site]/admin/config/openclinic/views/test
  or: Administration > Configuration > Open clinic > Open clinic views test
  and to test views creation.


MAINTAINERS
-----------

Current maintainers:
 * Sergey Loginov (goodboy) - https://drupal.org/user/222910
