<?php

namespace Drupal\openclinic_views\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\views\Entity\View;
use Drupal\views\Views;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views_ui\ViewUI;
use Drupal\views\Plugin\ViewsPluginManager;

/**
 * Class ViewsController.
 */
class ViewsController extends ControllerBase {

  /**
   * The base table connected with the wizard.
   *
   * @var string
   */
  protected $baseTable;

  /**
   * The entity type connected with the wizard.
   *
   * There might be base tables connected with entity types, if not this would
   * be empty.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Contains the information from entity_get_info of the $entity_type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * Entity id.
   *
   * @var string
   */
  protected $entityId;

  /**
   * Views items configuration arrays for filters added by the wizard.
   *
   * @var array
   */
  protected $filters = [];

  /**
   * Views items configuration arrays for sorts added by the wizard.
   *
   * @var array
   */
  protected $sorts = [];

  /**
   * Title displays of views.
   *
   * @var string
   */
  protected $title;

  /**
   * The wizard plugin manager.
   *
   * @var \Drupal\views\Plugin\ViewsPluginManager
   */
  protected $wizardManager;

  /**
   * Constructs a new ViewController object.
   *
   * @param \Drupal\views\Plugin\ViewsPluginManager $wizard_manager
   *   The wizard plugin manager.
   */
  public function __construct(ViewsPluginManager $wizard_manager) {
    $this->wizardManager = $wizard_manager;
  }

  /**
   * Default values for filters.
   *
   * By default, filters are not exposed and added to the first non-reserved
   * filter group.
   *
   * @var array
   */
  protected $filterDefaults = [
    'id' => NULL,
    'expose' => ['operator' => FALSE],
    'group' => 1,
  ];

  /**
   * Make view ID.
   *
   * @param string $type
   *   Entity type (node, taxonomy_term, ...).
   * @param string $id
   *   Entity id.
   *
   * @return string
   *   ID string.
   */
  public static function makeId($type, $id) {
    return 'clinic_' . $type . '_' . $id;
  }

  /**
   * Create view.
   *
   * @param string $type
   *   Entity type (node, taxonomy_term, ...).
   * @param string $id
   *   Entity id.
   */
  public function createView($type, $id) {
    $viewId = self::makeId($type, $id);
    $this->title = "Open clinic $type $id";

    // Check for existing view.
    $view = View::load($viewId);
    if ($view) {
      // $view->delete();
      $this->messenger()->addMessage($this->t('View @id already exists', ['@id' => $viewId]), 'warning');
      return FALSE;
    }

    $entity_types = $this->entityTypeManager()->getDefinitions();
    $this->entityType = $entity_types[$type];
    if (empty($this->entityType)) {
      $this->messenger()->addMessage($this->t('Entity type @type not found.', ['@type' => $type]), 'error');
      return FALSE;
    }

    $this->entityTypeId = $type;
    $this->entityId = $id;
    $this->baseTable = $this->entityType->getDataTable();

    $values = [
      'id' => $viewId,
      'label' => $this->title,
      'description' => 'Open clinic ' . $this->title,
      'baseTable' => $this->baseTable,
      'langcode' => $this->languageManager()->getDefaultLanguage()->getId(),
    ];

    // Views entity object.
    $view = View::create($values);

    $wizard_instance = $this->wizardManager->createInstance($this->entityTypeId);
    $display_options = $this->buildDisplayOptions($wizard_instance);

    /*
    // Allow the fully built options to be altered. This happens before adding
    // the options to the view, so that once they are eventually added we will
    // be able to get all the overrides correct.
    $this->alterDisplayOptions($display_options);
     */
    $this->addDisplays($view, $display_options);

    $viewUI = new ViewUI($view);
    $viewUI->save();

    return $viewId;
  }

  /**
   * Builds an array of display options for the view.
   *
   * @param object $wizard_instance
   *   Wizard instance.
   *
   * @return array
   *   An array whose keys are the names of each display and whose values are
   *   arrays of options for that display.
   */
  protected function buildDisplayOptions($wizard_instance) {
    // Display: Master.
    $display_options['default'] = $this->defaultDisplayOptions();
    $display_options['default'] = array_merge($display_options['default'], $this->reflection($wizard_instance, 'defaultDisplayOptions'));

    // Override display options.
    $display_options['row'] = [
      'type' => 'entity:' . $this->entityTypeId,
      'options' => ['view_mode' => 'teaser'],
    ];

    $display_options['default'] += [
      'filters' => [],
      'sorts' => [],
    ];

    $display_options['default']['filters'] += $this->defaultDisplayFilters();
    $display_options['default']['sorts'] += $this->defaultDisplaySorts();

    // Page.
    $pagePath = str_replace('clinic_', '', $this->entityId);
    $page = [
      'title' => $this->title,
      'path' => $pagePath,
      'style' => [
        'style_plugin' => 'default',
        'row_plugin' => 'teasers',
      ],
      'items_per_page' => 10,
      'pager' => TRUE,
    ];
    $display_options['page'] = $this->pageDisplayOptions($page);

    // Block.
    if ($this->moduleHandler()->moduleExists('block')) {
      $block = [
        'title' => $this->title,
        'style' => [
          'style_plugin' => 'default',
          'row_plugin' => 'teasers',
        ],
        'items_per_page' => 5,
      ];
      $display_options['block'] = $this->blockDisplayOptions($block);
    }

    // REST export.
    if ($this->moduleHandler()->moduleExists('rest')) {
      $pagePath = 'rest/' . str_replace('clinic_', '', $this->entityId);
      $rest = ['path' => $pagePath];
      $display_options['rest_export'] = $this->restExportDisplayOptions($rest);
    }

    return $display_options;
  }

  /**
   * Assembles the default display options for the view.
   *
   * Most wizards will need to override this method to provide some fields
   * or a different row plugin.
   *
   * @return array
   *   Returns an array of display options.
   */
  protected function defaultDisplayOptions() {
    $display_options = [];
    $display_options['access']['type'] = 'none';
    $display_options['cache']['type'] = 'tag';
    $display_options['query']['type'] = 'views_query';
    $display_options['exposed_form']['type'] = 'basic';
    $display_options['pager']['type'] = 'mini';
    $display_options['style']['type'] = 'default';
    $display_options['row']['type'] = 'fields';

    // Add default options array to each plugin type.
    foreach ($display_options as &$options) {
      $options['options'] = [];
    }

    // Add a least one field so the view validates and the user has a preview.
    // The base field can provide a default in its base settings; otherwise,
    // choose the first field with a field handler.
    $default_table = $this->baseTable;
    $data = Views::viewsData()->get($default_table);
    if (isset($data['table']['base']['defaults']['field'])) {
      $default_field = $data['table']['base']['defaults']['field'];
      // If the table for the default field is different to the base table,
      // load the view table data for this table.
      if (isset($data['table']['base']['defaults']['table']) && $data['table']['base']['defaults']['table'] != $default_table) {
        $default_table = $data['table']['base']['defaults']['table'];
        $data = Views::viewsData()->get($default_table);
      }
    }
    else {
      foreach ($data as $default_field => $field_data) {
        if (isset($field_data['field']['id'])) {
          break;
        }
      }
    }
    // @todo Refactor the code to use ViewExecutable::addHandler. See
    //   https://www.drupal.org/node/2383157.
    if (isset($default_field)) {
      $display_options['fields'][$default_field] = [
        'table' => $default_table,
        'field' => $default_field,
        'id' => $default_field,
        'entity_type' => isset($data[$default_field]['entity type']) ? $data[$default_field]['entity type'] : NULL,
        'entity_field' => isset($data[$default_field]['entity field']) ? $data[$default_field]['entity field'] : NULL,
        'plugin_id' => $data[$default_field]['field']['id'],
      ];
    }

    return $display_options;
  }

  /**
   * Retrieves all filter information used by the default display.
   *
   * Additional to the one provided by the plugin this method takes care about
   * adding additional filters based on user input.
   *
   * @return array
   *   An array of filter arrays keyed by ID. A sort array contains the options
   *   accepted by a filter handler.
   */
  protected function defaultDisplayFilters() {
    $filters = [];

    // Add any filters provided by the plugin.
    foreach ($this->getFilters() as $name => $info) {
      $filters[$name] = $info;
    }

    // Add content type filter.
    if ($this->entityTypeId == 'node') {
      $filters['type'] = [
        'id' => 'type',
        'table' => 'node_field_data',
        'field' => 'type',
        'relationship' => 'none',
        'group_type' => 'group',
        'admin_label' => '',
        'operator' => 'in',
        'value' => [$this->entityId => $this->entityId],
        'group' => 1,
        'exposed' => '',
        'expose' => [
          'operator_id' => '',
          'label' => '',
          'description' => '',
          'use_operator' => '',
          'operator' => '',
          'identifier' => '',
          'required' => '',
          'remember' => '',
          'multiple' => '',
          'remember_roles' => ['authenticated' => 'authenticated'],
          'reduce' => '',
        ],
        'is_grouped' => '',
        'group_info' => [
          'label' => '',
          'description' => '',
          'identifier' => '',
          'optional' => 1,
          'widget' => 'select',
          'multiple' => '',
          'remember' => '',
          'default_group' => 'All',
          'default_group_multiple' => [],
          'group_items' => [],
        ],
        'entity_type' => 'node',
        'entity_field' => 'type',
        'plugin_id' => 'bundle',
      ];
    }

    return $filters;
  }

  /**
   * Gets the filters property.
   *
   * @return array
   *   Filters.
   */
  public function getFilters() {
    $filters = [];

    // Add a default filter on the publishing status field, if available.
    if ($this->entityType && is_subclass_of($this->entityType->getClass(), EntityPublishedInterface::class)) {
      $field_name = $this->entityType->getKey('published');
      $this->filters = [
        $field_name => [
          'value' => TRUE,
          'table' => $this->baseTable,
          'field' => $field_name,
          'plugin_id' => 'boolean',
          'entity_type' => $this->entityTypeId,
          'entity_field' => $field_name,
        ],
      ] + $this->filters;
    }

    $default = $this->filterDefaults;

    foreach ($this->filters as $name => $info) {
      $default['id'] = $name;
      $filters[$name] = $info + $default;
    }

    return $filters;
  }

  /**
   * Retrieves all sort information used by the default display.
   *
   * Additional to the one provided by the plugin this method takes care about
   * adding additional sorts based on user input.
   *
   * @return array
   *   An array of sort arrays keyed by ID. A sort array contains the options
   *   accepted by a sort handler.
   */
  protected function defaultDisplaySorts() {
    $sorts = [];

    // Add any sorts provided by the plugin.
    foreach ($this->getSorts() as $name => $info) {
      $sorts[$name] = $info;
    }

    // Overriding for node entity.
    // todo: find properly code generate.
    if (empty($sorts) && $this->entityTypeId == 'node') {
      $sorts['created'] = [
        'id' => 'created',
        'table' => 'node_field_data',
        'field' => 'created',
        'order' => 'DESC',
        'entity_type' => 'node',
        'entity_field' => 'created',
        'plugin_id' => 'date',
        'relationship' => 'none',
        'group_type' => 'group',
        'admin_label' => '',
        'exposed' => '',
        'expose' => ['label' => ''],
        'granularity' => 'second',
      ];
    }

    return $sorts;
  }

  /**
   * Gets the sorts property.
   *
   * @return array
   *   Sorts.
   */
  public function getSorts() {
    return $this->sorts;
  }

  /**
   * Retrieves the page display options.
   *
   * @param array $page
   *   Array page.
   *
   * @return array
   *   Returns an array of display options.
   */
  protected function pageDisplayOptions(array $page) {
    $display_options = [];

    $display_options['title'] = $page['title'];
    $display_options['path'] = $page['path'];
    $display_options['style'] = ['type' => $page['style']['style_plugin']];
    // Not every style plugin supports row style plugins.
    // Make sure that the selected row plugin is a valid one.
    $options = $this->rowStyleOptions();
    $display_options['row'] = ['type' => (isset($page['style']['row_plugin']) && isset($options[$page['style']['row_plugin']])) ? $page['style']['row_plugin'] : 'fields'];

    // If the specific 0 items per page, use no pager.
    if (empty($page['items_per_page'])) {
      $display_options['pager']['type'] = 'none';
    }
    // If the user checked the pager checkbox use a mini pager.
    elseif (!empty($page['pager'])) {
      $display_options['pager']['type'] = 'mini';
    }
    // If the user doesn't have checked the checkbox use the pager which just
    // displays a certain amount of items.
    else {
      $display_options['pager']['type'] = 'some';
    }
    $display_options['pager']['options']['items_per_page'] = $page['items_per_page'];

    // Generate the menu links settings if the user checked the link checkbox.
    if (!empty($page['link'])) {
      $display_options['menu']['type'] = 'normal';
      $display_options['menu']['title'] = $page['link_properties']['title'];
      $display_options['menu']['menu_name'] = $page['link_properties']['menu_name'];
    }
    return $display_options;
  }

  /**
   * Retrieves the block display options.
   *
   * @param array $block
   *   Array block.
   *
   * @return array
   *   Returns an array of display options.
   */
  protected function blockDisplayOptions(array $block) {
    $display_options = [];

    $display_options['title'] = $block['title'];
    $display_options['style'] = ['type' => $block['style']['style_plugin']];
    $options = $this->rowStyleOptions();
    $display_options['row'] = ['type' => (isset($block['style']['row_plugin']) && isset($options[$block['style']['row_plugin']])) ? $block['style']['row_plugin'] : 'fields'];
    $display_options['pager']['type'] = $block['pager'] ? 'full' : (empty($block['items_per_page']) ? 'none' : 'some');
    $display_options['pager']['options']['items_per_page'] = $block['items_per_page'];
    return $display_options;
  }

  /**
   * Retrieves the REST export display options from the submitted form values.
   *
   * @param array $rest
   *   Array REST.
   *
   * @return array
   *   Returns an array of display options.
   */
  protected function restExportDisplayOptions(array $rest) {
    $display_options = [];
    $display_options['path'] = $rest['path'];
    $display_options['style'] = ['type' => 'serializer'];

    return $display_options;
  }

  /**
   * Adds the array of display options to the view, with appropriate overrides.
   */
  protected function addDisplays(View $view, $display_options) {
    // Initialize and store the view executable to get the display plugin
    // instances.
    $executable = $view->getExecutable();

    // Display: Master.
    $default_display = $executable->newDisplay('default', 'Master', 'default');
    foreach ($display_options['default'] as $option => $value) {
      $default_display->setOption($option, $value);
    }

    // Display: Page.
    if (isset($display_options['page'])) {
      $display = $executable->newDisplay('page', 'Page', 'page_1');
      // The page display is usually the main one (from the user's point of
      // view). Its options should therefore become the overall view defaults,
      // so that new displays which are added later automatically inherit them.
      $this->setDefaultOptions($display_options['page'], $display, $default_display);

      // Display: Feed (attached to the page).
      if (isset($display_options['feed'])) {
        $display = $executable->newDisplay('feed', 'Feed', 'feed_1');
        $this->setOverrideOptions($display_options['feed'], $display, $default_display);
      }
    }

    // Display: Block.
    if (isset($display_options['block'])) {
      $display = $executable->newDisplay('block', 'Block', 'block_1');
      // When there is no page, the block display options should become the
      // overall view defaults.
      if (!isset($display_options['page'])) {
        $this->setDefaultOptions($display_options['block'], $display, $default_display);
      }
      else {
        $this->setOverrideOptions($display_options['block'], $display, $default_display);
      }
    }

    // Display: REST export.
    if (isset($display_options['rest_export'])) {
      $display = $executable->newDisplay('rest_export', 'REST export', 'rest_export_1');
      // If there is no page or block, the REST export display options should
      // become the overall view defaults.
      if (!isset($display_options['page']) && !isset($display_options['block'])) {
        $this->setDefaultOptions($display_options['rest_export'], $display, $default_display);
      }
      else {
        $this->setOverrideOptions($display_options['rest_export'], $display, $default_display);
      }
    }

    // Initialize displays and merge all plugin default values.
    $executable->mergeDefaults();
  }

  /**
   * Sets options for a display and makes them the default options if possible.
   *
   * This function can be used to set options for a display when it is desired
   * that the options also become the defaults for the view whenever possible.
   * This should be done for the "primary" display created in the view wizard,
   * so that new displays which the user adds later will be similar to this
   * one.
   *
   * @param array $options
   *   An array whose keys are the name of each option and whose values are the
   *   desired values to set.
   * @param \Drupal\views\Plugin\views\display\DisplayPluginBase $display
   *   The display handler which the options will be applied to. The default
   *   display will actually be assigned the options (and this display will
   *   inherit them) when possible.
   * @param \Drupal\views\Plugin\views\display\DisplayPluginBase $default_display
   *   The default display handler, which will store the options when possible.
   */
  protected function setDefaultOptions(array $options, DisplayPluginBase $display, DisplayPluginBase $default_display) {
    foreach ($options as $option => $value) {
      // If the default display supports this option, set the value there.
      // Otherwise, set it on the provided display.
      $default_value = $default_display->getOption($option);
      if (isset($default_value)) {
        $default_display->setOption($option, $value);
      }
      else {
        $display->setOption($option, $value);
      }
    }
  }

  /**
   * Sets options for a display, inheriting from the defaults when possible.
   *
   * This function can be used to set options for a display when it is desired
   * that the options inherit from the default display whenever possible. This
   * avoids setting too many options as overrides, which will be harder for the
   * user to modify later. For example, if $this->setDefaultOptions() was
   * previously called on a page display and then this function is called on a
   * block display, and if the user entered the same title for both displays in
   * the views wizard, then the view will wind up with the title stored as the
   * default (with the page and block both inheriting from it).
   *
   * @param array $options
   *   An array whose keys are the name of each option and whose values are the
   *   desired values to set.
   * @param \Drupal\views\Plugin\views\display\DisplayPluginBase $display
   *   The display handler which the options will be applied to. The default
   *   display will actually be assigned the options (and this display will
   *   inherit them) when possible.
   * @param \Drupal\views\Plugin\views\display\DisplayPluginBase $default_display
   *   The default display handler, which will store the options when possible.
   */
  protected function setOverrideOptions(array $options, DisplayPluginBase $display, DisplayPluginBase $default_display) {
    foreach ($options as $option => $value) {
      // Only override the default value if it is different from the value that
      // was provided.
      $default_value = $default_display->getOption($option);
      if (!isset($default_value)) {
        $display->setOption($option, $value);
      }
      elseif ($default_value !== $value) {
        $display->overrideOption($option, $value);
      }
    }
  }

  /**
   * Retrieves row style plugin names.
   *
   * @return array
   *   Returns the plugin names available for the base table of the wizard.
   */
  protected function rowStyleOptions() {
    // Get all available row plugins by default.
    $options = Views::fetchPluginNames('row', 'normal', [$this->baseTable]);
    return $options;
  }

  /**
   * Retrieves reflection method for access proctected methods.
   *
   * @param object $instance
   *   Class instance.
   * @param string $method
   *   Protected method name.
   *
   * @return object
   *   Returns protected method value.
   */
  private function reflection($instance, $method) {
    $class = get_class($instance);
    $reflection_method = new \ReflectionMethod($class, $method);
    $reflection_method->setAccessible(TRUE);
    return $reflection_method->invoke($instance);
  }

}
