<?php

namespace Drupal\openclinic_views\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openclinic_views\Controller\ViewsController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class TestForm.
 */
class TestForm extends FormBase {

  /**
   * Views manager.
   *
   * @var \Drupal\openclinic_views\Controller\ViewsController
   */
  protected $viewsManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TestForm object.
   *
   * @param \Drupal\openclinic_views\Controller\ViewsController $views_manager
   *   Views manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ViewsController $views_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->viewsManager = $views_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openclinic_views'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entities = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $entities = array_filter($entities, function ($key) {
      return !in_array($key, ['article', 'page']);
    }, ARRAY_FILTER_USE_KEY);

    if (empty($entities)) {
      $this->messenger()->addMessage($this->t('No node type for views creation'), 'error');
      return;
    }

    // Node types.
    $form['node_types'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Node types'),
    ];

    foreach (array_keys($entities) as $value) {
      $form['node_types'][$value] = [
        '#type' => 'checkbox',
        '#title' => $value,
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getGroups() as $group => $value) {
      if (in_array($group, ['vocabularies', 'node_types'])) {
        continue;
      }
      if (!$form_state->getValue($group)) {
        continue;
      }

      $viewId = $this->viewsManager->createView('node', $group);
      if ($viewId) {
        $link = Link::createFromRoute($this->t('View @id created', ['@id' => $viewId]), 'entity.view.edit_form', ['view' => $viewId]);
        $this->messenger()->addMessage($link);
      }
      else {
        $this->messenger()->addMessage($this->t('View @id isnot created', ['@id' => $group]), 'warning');
      }
    }
  }

}
