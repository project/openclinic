<?php

namespace Drupal\openclinic_install\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\openclinic_views\Controller\ViewsController;
use Drupal\views\Entity\View;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ResetForm.
 */
class ResetForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Constructs a new ResetForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->batchBuilder = new BatchBuilder();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reset_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['info'] = [
      '#markup' => '<h2>' . $this->t('Checked entities will be deleted.') . '</h2>',
    ];

    // Vocabularies.
    $vocabulariesDefault = ['tags'];
    $form['vocabularies'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Vocabularies'),
    ];
    $entities = Vocabulary::loadMultiple();
    foreach (array_keys($entities) as $value) {
      $form['vocabularies']['v__' . $value] = [
        '#type' => 'checkbox',
        '#title' => $value,
        '#default_value' => !in_array($value, $vocabulariesDefault),
      ];
    }

    // Node types.
    $nodeTypesDefault = ['article', 'page'];
    $form['node_types'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Node types'),
    ];
    $entities = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach (array_keys($entities) as $value) {
      $form['node_types']['n__' . $value] = [
        '#type' => 'checkbox',
        '#title' => $value,
        '#default_value' => !in_array($value, $nodeTypesDefault),
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $processed = FALSE;
    $this->batchBuilder
      ->setTitle($this->t('Entities deleting'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));
    $this->batchBuilder->setFile(drupal_get_path('module', 'openclinic_install') . '/src/Form/ResetForm.php');

    $vocabularies = Vocabulary::loadMultiple();
    $nodeTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($form_state->getGroups() as $group => $value) {
      if (in_array($group, ['vocabularies', 'node_types'])) {
        continue;
      }
      if (!$form_state->getValue($group)) {
        continue;
      }

      $ids = explode('__', $group);
      if (count($ids) > 1) {
        $id = $ids[1];
        switch ($ids[0]) {
          case 'v':
            $entity = $vocabularies[$id];
            if ($entity) {
              $processed = TRUE;
              $this->batchBuilder->addOperation([$this, 'processDelete'], [$entity]);
              $this->messenger()->addMessage($this->t('Vocabulary @name was deleted.', ['@name' => $id]));
            }
            break;

          case 'n':
            $entity = $nodeTypes[$id];
            if ($entity) {
              $processed = TRUE;
              $this->batchBuilder->addOperation([$this, 'processDelete'], [$entity]);
              $this->messenger()->addMessage($this->t('Node type @name was deleted.', ['@name' => $id]));
            }
            break;
        }
      }
    }

    if ($processed) {
      $this->batchBuilder->setFinishCallback([$this, 'processFinished']);
      batch_set($this->batchBuilder->toArray());
    }

  }

  /**
   * Batch entity delete.
   *
   * @param object $entity
   *   Entity for deleting.
   * @param array $context
   *   Batch context.
   */
  public function processDelete($entity, array &$context) {
    // View deleting, if exists.
    if ($this->moduleHandler->moduleExists('openclinic_views')) {
      if ($entity->getEntityTypeId() == 'node_type') {
        $viewId = ViewsController::makeId('node', $entity->id());
        $view = View::load($viewId);
        if ($view) {
          $view->delete();
          $this->messenger()->addMessage($this->t('View @name was deleted.', ['@name' => $viewId]));
        }
      }
    }

    // Entity delete.
    $entity->delete();
  }

  /**
   * Finished callback for batch.
   */
  public function processFinished($success, $results, $operations) {
    $message = $this->t('Entities was deleted');
    $this->messenger()->addStatus($message);
  }

}
