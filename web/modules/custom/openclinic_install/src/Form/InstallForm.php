<?php

namespace Drupal\openclinic_install\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\NodeType;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Batch\BatchBuilder;

/**
 * Class InstallForm.
 */
class InstallForm extends FormBase {

  protected $termDelimiter = '-';

  /**
   * IP Manager variable.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepository
   */
  protected $entityDisplayRepository;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Openclinic views object.
   *
   * @var Drupal\openclinic_views\ViewsController
   */
  protected $viewsManager;

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Constructs a new InstallForm object.
   */
  public function __construct() {
    $this->batchBuilder = new BatchBuilder();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    $instance->fieldTypeManager = $container->get('plugin.manager.field.field_type');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->viewsManager = NULL;
    if ($instance->moduleHandler->moduleExists('openclinic_views')) {
      $instance->viewsManager = $container->get('openclinic_views');
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'install_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $data = $this->getEntitiesList(NULL, TRUE);
    if (empty($data)) {
      $url = Link::fromTextAndUrl('configuration', Url::fromRoute('openclinic.config'))->toString();
      $this->messenger()->addMessage($this->t('No data for install. Go to the @url', ['@url' => $url]), 'error');
      return;
    }

    $vocabularies = $this->getEntitiesList('vocabulary');
    $nodeTypes = $this->getEntitiesList('node');
    if (empty($vocabularies) && empty($nodeTypes)) {
      $config_link = Link::fromTextAndUrl('configuration', Url::fromRoute('openclinic.config'))->toString();
      $this->messenger()->addMessage($this->t('No data for install. Need @config_link', ['@config_link' => $config_link]), 'error');
      return;
    }

    $contentExists = $this->getContentTypesExists();
    $contentTypesExists = $contentExists['all'];
    $vocabulariesExists = $contentExists['vocabulary'];
    $nodeTypesExists = $contentExists['node_type'];

    $form = [];

    // Phase installation.
    $phase = $this->getPhase();
    $form['phase'] = [
      '#markup' => $this->t('Phase: @phase / @total', [
        '@phase' => $phase['current'],
        '@total' => $phase['total'],
      ]),
    ];

    // Content types markup.
    $contentTypesExistsArray = [];
    if (!empty($vocabulariesExists)) {
      foreach ($vocabulariesExists as $key => $taxonomy_vocabulary) {
        $vocabulariesExists[$key] = Link::fromTextAndUrl($taxonomy_vocabulary, Url::fromUserInput("/admin/structure/taxonomy/manage/$taxonomy_vocabulary/overview"))->toString();
      }
      $contentTypesExistsArray['Vocabularies'] = implode(', ', $vocabulariesExists);
    }
    if (!empty($nodeTypesExists)) {
      foreach ($nodeTypesExists as $key => $node_type) {
        $nodeTypesExists[$key] = Link::fromTextAndUrl($node_type, Url::fromUserInput("/admin/structure/types/manage/$node_type"))->toString();
      }
      $contentTypesExistsArray['Node types'] = implode(', ', $nodeTypesExists);
    }
    if (!empty($contentTypesExistsArray)) {
      foreach ($contentTypesExistsArray as $key => $value) {
        $name = $this->t('@key:', ['@key' => $key]);
        $contentTypesExistsArray[$key] = $name . ' ' . $value;
      }
      $contentTypesExistsMarkup = implode('<br/>', $contentTypesExistsArray);

      $form['contentTypesExists'] = [
        '#markup' => $contentTypesExistsMarkup,
        '#allowed_tags' => ['a', 'br'],
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
    }

    // Vocabularies.
    $show_group = FALSE;
    foreach ($vocabularies as $value) {
      if ($contentTypesExists['vocabulary'][$value] || !$this->getRelationsExists('vocabulary', $value, $contentTypesExists)) {
        continue;
      }

      if (!$show_group) {
        $form['vocabularies'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Vocabularies'),
        ];
        $show_group = TRUE;
      }

      $form['vocabularies']['v__' . $value] = [
        '#type' => 'textarea',
        '#title' => $value,
        '#default_value' => "item1\n-item1.1\n-item1.2\n--item1.2.1\n-item1.3\nitem2\n-item2.1",
      ];
    }

    // Node types.
    $show_group = FALSE;
    foreach ($nodeTypes as $value) {
      if ($contentTypesExists['node'][$value] || !$this->getRelationsExists('node', $value, $contentTypesExists)) {
        continue;
      }

      if (!$show_group) {
        $form['node_types'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Node types'),
        ];
        $show_group = TRUE;
      }

      $form['node_types']['n__' . $value] = [
        '#type' => 'checkbox',
        '#title' => $value,
        '#default_value' => TRUE,
      ];
    }

    // Submit button checking.
    if ((count($vocabularies) + count($nodeTypes)) > (count($vocabulariesExists) + count($nodeTypesExists))) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Install'),
      ];
    }
    else {
      $form['submit_message'] = [
        '#markup' => $this->t('Complete!'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $processed = FALSE;
    $this->batchBuilder
      ->setTitle($this->t('Entities creating'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));
    $this->batchBuilder->setFile(drupal_get_path('module', 'openclinic_install') . '/src/Form/InstallForm.php');

    foreach ($form_state->getGroups() as $group => $value) {
      if (in_array($group, ['vocabularies', 'node_types'])) {
        continue;
      }

      $ids = explode('__', $group);
      if (count($ids) > 1) {
        $id = $ids[1];
        switch ($ids[0]) {
          case 'v':
            $vocabulary_data = trim($form_state->getValue($group));
            if (empty($vocabulary_data)) {
              continue;
            }
            $processed = TRUE;

            $this->batchBuilder->addOperation([$this, 'processImportTaxonomy'], [$id, $vocabulary_data]);
            break;

          case 'n':
            if (!$form_state->getValue($group)) {
              continue;
            }
            $processed = TRUE;
            $this->batchBuilder->addOperation([$this, 'processImportNodes'], [$id]);
            break;
        }
      }
    }

    if ($processed) {
      $this->batchBuilder->setFinishCallback([$this, 'processFinished']);
      batch_set($this->batchBuilder->toArray());
    }
  }

  /**
   * Batch processed taxonomy.
   *
   * @param string $id
   *   Entity id.
   * @param string $vocabulary_data
   *   Vocabulary data.
   * @param array $context
   *   Batch context.
   */
  public function processImportTaxonomy($id, $vocabulary_data, array &$context) {
    // Taxonomy import.
    $this->importTaxonomy([
      'data' => $vocabulary_data,
      'vid' => $id,
      'name' => $id,
      'description' => $this->t('Open clinic @name vocabulary.', [
        '@name' => $id,
      ]),
    ]);

    // Add field to related entities.
    $fields = $this->getFieldsEntity('vocabulary', $id, TRUE);
    if (!empty($fields)) {
      foreach ($fields as $field) {
        $this->nodeFieldCreate($field);
      }
    }
  }

  /**
   * Batch processed node type.
   *
   * @param string $id
   *   Entity id.
   * @param array $context
   *   Batch context.
   */
  public function processImportNodes($id, array &$context) {
    $fields = $this->getFieldsEntity('node', $id);
    $this->importNodes([
      'type' => $id,
      'name' => $id,
      'description' => $this->t('Open clinic @name node type.', [
        '@name' => $id,
      ]),
    ], $fields);

    // View creating, if enable.
    if ($this->moduleHandler->moduleExists('openclinic_views')) {
      $viewId = $this->viewsManager->createView('node', $id);
      if ($viewId) {
        $msg = Link::createFromRoute($this->t('View @id created', ['@id' => $id]), 'entity.view.edit_form', ['view' => $viewId]);
        $this->messenger()->addMessage($msg);
      }
    }
  }

  /**
   * Finished callback for batch.
   */
  public function processFinished($success, $results, $operations) {
    $message = $this->t('Entities was created');
    $this->messenger()->addStatus($message);
  }

  /**
   * Import Open clinic taxonomy.
   *
   * @param array $options
   *   Taxonomy definitions.
   */
  protected function importTaxonomy(array $options) {
    // Parameters.
    $data = $options['data'];
    $ocabulary_id = $options['vid'];
    $vocabulary_name = $options['name'];
    $vocabulary_description = $options['description'];
    $structure_arr = explode("\n", trim($data));
    if (!empty($structure_arr)) {
      if (!$this->existsEntityType($ocabulary_id, 'vocabulary')) {
        // Create openclinic vocabulary.
        $vocabulary = Vocabulary::create([
          'vid' => $ocabulary_id,
          'description' => $vocabulary_description,
          'name' => $vocabulary_name,
        ]);
        $vocabulary->enforceIsNew();
        $vocabulary->save();
        $this->messenger()->addMessage($this->t('Import taxonomy: vocabulary @name created.', ['@name' => $vocabulary_name]));
      }

      // Parse service structure.
      $items = [];
      foreach ($structure_arr as $item) {
        $item = trim($item);
        $level = 0;
        $chars = preg_split('//u', $item, NULL, PREG_SPLIT_NO_EMPTY);
        foreach ($chars as $char) {
          if ($this->termDelimiter == $char) {
            $level++;
          }
          else {
            break;
          }
        }

        if ($level > 0) {
          $parts = explode(str_repeat($this->termDelimiter, $level), $item);
          $name = trim($parts[1]);
        }
        else {
          $name = $item;
        }
        $items[] = ['name' => $name, 'level' => $level];
      }

      // Check for the first item.
      if ($items[0]['level'] > 0) {
        $this->messenger()->addMessage($this->t('Import taxonomy: wrong level for first item.'), 'error');
        return;
      }

      // Find parents.
      foreach ($items as $key => $item) {
        $level = $item['level'];
        if (!$level) {
          $parent = NULL;
        }
        else {
          // Seek for nearest parent.
          $parent_key = $key - 1;
          while ($parent_key >= 0 && $items[$parent_key]['level'] >= $level) {
            $parent_key--;
          }
          $parent = $parent_key;
        }
        $items[$key]['parent'] = $parent;
      }

      // Create terms.
      $ids = [];
      foreach ($items as $key => $item) {
        unset($item['level']);

        $term_create_arr = [
          'vid' => $ocabulary_id,
          'name' => $item['name'],
        ];

        $parent = $item['parent'];
        if ($parent >= 0 && isset($ids[$parent])) {
          $term_create_arr['parent'] = [$ids[$parent]];
        }

        $new_term = Term::create($term_create_arr);
        $new_term->enforceIsNew();
        $new_term->save();
        $this->messenger()->addMessage($this->t('Import taxonomy: create term @name', ['@name' => $item['name']]));

        $ids[$key] = $new_term->id();
      }
    }
  }

  /**
   * Create Open clinic node type.
   *
   * @param array $options
   *   Node definitions.
   * @param array $fields
   *   Field definitions.
   * @param array $extra
   *   Extra options.
   */
  protected function importNodes(array $options, array $fields, array $extra = []) {
    $nodeType = NodeType::create($options);
    $nodeType->enforceIsNew();
    $nodeType->save();
    $this->messenger()->addMessage($this->t('@name node type was created.', [
      '@name' => $options['name'],
    ]));

    if (empty($extra['no_body'])) {
      // Create body field.
      node_add_body_field($nodeType, $this->t('Description'));
    }

    // Create fields.
    foreach ($fields as $name => $field) {
      $field['name'] = $name;
      $field['bundle'] = $options['type'];
      $this->nodeFieldCreate($field);
    }
  }

  /**
   * Create Field.
   *
   * @param array $field
   *   Options for node create: field storage, form display, view display.
   */
  protected function nodeFieldCreate(array $field) {
    if (!isset($field['name'])) {
      $this->messenger()->addMessage($this->t('No name key of field'), 'error');
      return;
    }

    // Teaser using.
    if (isset($field['teaser'])) {
      $teaser = (bool) $field['teaser'];
      unset($field['teaser']);
    }
    else {
      $teaser = FALSE;
    }

    // Field storage.
    $fieldStorage = FieldStorageConfig::loadByName('node', $field['name']);
    if (empty($fieldStorage)) {
      $field_storage_options = [
        'field_name' => $field['name'],
        'entity_type' => 'node',
        'type' => $field['type'],
        'cardinality' => $field['cardinality'] ?? 1,
      ];
      if (isset($field['settings_storage'])) {
        $field_storage_options['settings'] = $field['settings_storage'];
      }
      $fieldStorage = FieldStorageConfig::create($field_storage_options);
      $fieldStorage->save();
    }

    // Field config.
    $field_options = [
      'field_storage' => $fieldStorage,
      'bundle' => $field['bundle'],
      'label' => $field['label'],
    ];
    if (isset($field['settings_field'])) {
      $field_options['settings'] = $field['settings_field'];
    }
    FieldConfig::create($field_options)->save();

    // Form display.
    if (!isset($field['form_display'])) {
      $field['form_display'] = [];
    }

    $this->entityDisplayRepository
      ->getFormDisplay('node', $field['bundle'])
      ->setComponent($field['name'], $field['form_display'])
      ->save();

    // View display.
    if (!isset($field['view_display'])) {
      $field['view_display'] = [];
    }

    $this->entityDisplayRepository
      ->getViewDisplay('node', $field['bundle'])
      ->setComponent($field['name'], $field['view_display'])
      ->save();

    // Add teaser view node.
    if ($teaser) {
      $view_modes = $this->entityDisplayRepository->getViewModes('node');
      if (isset($view_modes['teaser'])) {
        $this->entityDisplayRepository('node', $field['bundle'], 'teaser')
          ->setComponent($field['name'], [
            'label' => 'hidden',
            'type' => 'text_summary_or_trimmed',
          ])
          ->save();
      }
    }
  }

  /**
   * Check entity type exists.
   *
   * @param string $id
   *   Entity type id, e.g. doctors or service.
   * @param string $entityType
   *   Entity type: 'node' or 'vocabulary'.
   *
   * @return bool|array
   *   Entity type existing or all entities names list.
   */
  protected function existsEntityType($id, $entityType = 'node') {
    switch ($entityType) {
      case 'node':
      default:
        $entities = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
        break;

      case 'vocabulary':
        $entities = Vocabulary::loadMultiple();
        break;
    }

    if ($id) {
      return isset($entities[$id]);
    }
    else {
      $keys = array_keys($entities);
      return array_combine($keys, $keys);
    }
  }

  /**
   * Get default field options.
   *
   * @param string $type
   *   Field type: text, entity_reference.
   * @param array $optional
   *   Optional parameters for field definition.
   *
   * @return array
   *   Field options array.
   */
  protected function fieldDefaultOptions($type, array $optional = []) {
    $field = [];

    switch ($type) {
      case 'text':
        $field = [
          'type' => 'text',
          'form_display' => [
            'type' => 'text_textfield',
          ],
          'view_display' => [
            'type' => 'text_default',
          ],
        ];
        break;

      case 'taxonomy_reference':
        $field = [
          'type' => 'entity_reference',
          'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
          'settings_storage' => [
            'target_type' => 'taxonomy_term',
          ],
          'form_display' => [
            'type' => 'entity_reference_autocomplete',
          ],
        ];

        if (isset($optional['target'])) {
          $field['settings_field'] = [
            'handler' => 'default',
            'handler_settings' => [
              'target_bundles' => $optional['target'],
            ],
          ];
          unset($optional['target']);
        }
        break;

      case 'node_reference':
        $field = [
          'type' => 'entity_reference',
          'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
          'settings_storage' => [
            'target_type' => 'node',
          ],
          'form_display' => [
            'type' => 'entity_reference_autocomplete',
          ],
        ];

        if (isset($optional['target'])) {
          $field['settings_field'] = [
            'handler' => 'default',
            'handler_settings' => [
              'target_bundles' => $optional['target'],
            ],
          ];
          unset($optional['target']);
        }
        break;

      case 'text_with_summary':
        $field = [
          'type' => 'text_with_summary',
          'settings' => [
            'display_summary' => TRUE,
          ],
          'form_display' => [
            'type' => 'text_textarea_with_summary',
          ],
          'view_display' => [
            'label' => 'hidden',
            'type' => 'text_default',
          ],
        ];
        break;

      case 'image':
        $field = [
          'type' => 'image',
          'form_display' => [
            'type' => 'image_image',
          ],
        ];
        break;

      default:
        $field = [
          'type' => $type,
        ];
        break;
    }

    if (!isset($field['type'])) {
      return [];
    }
    return array_merge($field, $optional);
  }

  /**
   * Get phase installation.
   *
   * @return array
   *   Number phase (current, total).
   */
  protected function getPhase() {
    $phases = $this->getEntitiesList(NULL, TRUE)['phases'];

    $current = 0;
    $stop = FALSE;
    foreach ($phases as $num => $phase) {
      $current = $num + 1;
      foreach ($phase as $type => $items) {
        $phaseItemsExists = 0;
        foreach ($items as $item) {
          $exists = (int) $this->existsEntityType($item, $type);
          $phaseItemsExists += $exists;
        }
        if (!$phaseItemsExists) {
          $stop = TRUE;
          break;
        }
      }
      if ($stop) {
        break;
      }
    }

    $total = count($phases);
    return [
      'current' => $current,
      'total' => $total,
    ];
  }

  /**
   * Get entities list.
   *
   * @param string $type
   *   Entity type.
   * @param bool $raw
   *   Get raw phases list.
   *
   * @return array
   *   Array of entities.
   */
  protected function getEntitiesList($type = NULL, $raw = FALSE) {
    // Phases.
    $phases = $this->getData('phases');

    // Relations.
    $relations = $this->getData('relations');

    // Base fields.
    $base_fields = $this->getData('base_fields');

    if ($raw) {
      return [
        'phases' => $phases,
        'relations' => $relations,
        'base_fields' => $base_fields,
      ];
    }

    $entities = [];
    foreach ($phases as $phase) {
      foreach ($phase as $phaseType => $items) {
        if (empty($type) || $type == $phaseType) {
          foreach ($items as $item) {
            $entities[$phaseType][] = $item;
          }
        }
      }
    }
    return empty($type) ? $entities : current($entities);
  }

  /**
   * Relations exists check.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $entity_id
   *   Entity id.
   * @param array $contentTypesExists
   *   Content types exists array.
   *
   * @return bool
   *   Relations exists.
   */
  protected function getRelationsExists($entity_type, $entity_id, array $contentTypesExists) {
    $relations = $this->getEntitiesList(NULL, TRUE)['relations'];

    if (!empty($relations) && !empty($relations[$entity_type][$entity_id])) {
      foreach ($relations[$entity_type][$entity_id] as $relation_type => $items) {
        foreach ($items as $value) {
          if (!$contentTypesExists[$relation_type][$value]) {
            return FALSE;
          }
        }
      }
    }
    return TRUE;
  }

  /**
   * Get fields for node type imports.
   *
   * @param string $type
   *   Entity type.
   * @param string $id
   *   Entity id.
   * @param bool $callback
   *   Is callback.
   *
   * @return array
   *   Fields array.
   */
  protected function getFieldsEntity($type, $id, $callback = FALSE) {
    $fields = [];
    // List of the all existing types.
    $fieldTypesList = $this->fieldTypeManager->getUiDefinitions();

    // Get basic fields from node defintions.
    $base_fields = $this->getEntitiesList(NULL, TRUE)['base_fields'];
    if (!empty($base_fields) && !empty($base_fields[$type][$id])) {
      foreach ($base_fields[$type][$id] as $base_field_id => $base_field) {
        list($field_type, $field_label, $options) = $base_field;

        // Check field_type for existing.
        if (!isset($fieldTypesList[$field_type])) {
          $this->messenger()->addMessage($this->t('Field type @name not found.', ['@name' => $field_type]), 'error');
          continue;
        }

        $default_options = ['label' => $field_label];
        if (!empty($options)) {
          $default_options = array_merge($default_options, $options);
        }
        $fields[$base_field_id] = $this->fieldDefaultOptions($field_type, $default_options);
      }
    }

    // Get relations fields.
    $relations = $this->getEntitiesList(NULL, TRUE)['relations'];
    if (!empty($relations) && !empty($relations[$type][$id])) {
      foreach ($relations[$type][$id] as $relation_type => $items) {
        foreach ($items as $value) {
          if ($callback) {
            $callback_type = NULL;
            switch ($type) {
              case 'vocabulary':
                $callback_type = 'taxonomy_reference';
                break;

              case 'node':
                $callback_type = 'node_reference';
                break;
            }

            if ($callback_type) {
              $callback_field = $id . '_field';
              $fields[$callback_field] = $this->fieldDefaultOptions($callback_type, [
                'label' => $id,
                'target' => [$id],
              ]);
              $fields[$callback_field]['name'] = $callback_field;
              $fields[$callback_field]['bundle'] = $value;
            }
          }
          else {
            switch ($relation_type) {
              case 'vocabulary':
                $fields[$value . '_field'] = $this->fieldDefaultOptions('taxonomy_reference', [
                  'label' => $value,
                  'target' => [$value],
                ]);
                break;

              case 'node':
                $fields[$value . '_field'] = $this->fieldDefaultOptions('node_reference', [
                  'label' => $value,
                  'target' => [$value],
                ]);
                break;
            }
          }
        }
      }
    }

    return $fields;
  }

  /**
   * Get content types exists.
   *
   * @return array
   *   Array of all, vocabulary, node_type exists.
   */
  protected function getContentTypesExists() {
    $contentTypesExists = [];

    $vocabulariesExists = [];
    $vocabulariesAll = $this->existsEntityType(NULL, 'vocabulary');
    foreach ($this->getEntitiesList('vocabulary') as $value) {
      $exists = isset($vocabulariesAll[$value]);
      if ($exists) {
        $vocabulariesExists[] = $value;
      }
      $contentTypesExists['vocabulary'][$value] = $exists;
    }

    $nodeTypesExists = [];
    $nodeTypesAll = $this->existsEntityType(NULL, 'node');
    foreach ($this->getEntitiesList('node') as $value) {
      $exists = isset($nodeTypesAll[$value]);
      if ($exists) {
        $nodeTypesExists[] = $value;
      }
      $contentTypesExists['node'][$value] = $exists;
    }

    return [
      'all' => $contentTypesExists,
      'vocabulary' => $vocabulariesExists,
      'node_type' => $nodeTypesExists,
    ];
  }

  /**
   * Get data.
   *
   * @param string $name
   *   Data name.
   *
   * @return array
   *   Data array.
   */
  public function getData($name) {
    $config = $this->config('openclinic.config');
    $data = $config->get($name);
    if (!empty($data)) {
      return json_decode($data, TRUE);
    }
    return [];
  }

}
