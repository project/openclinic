INTRODUCTION
------------

Openclinic install module is a module for creating openclinic
entities and their derivatives (e.g. views) using Openclinic core
settings. The module has Reset form for deleting entities with
derivatives.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable the module.


CONFIGURATION
-------------

* Go to [Your Site]/admin/config/openclinic/install
  or: Administration > Configuration > Open clinic > Open clinic install
  and select entities for creating.

* Go to [Your Site]/admin/config/openclinic/install/reset
  or: Administration > Configuration > Open clinic > Open clinic install reset
  and select entities for deleting.


MAINTAINERS
-----------

Current maintainers:
 * Sergey Loginov (goodboy) - https://drupal.org/user/222910
