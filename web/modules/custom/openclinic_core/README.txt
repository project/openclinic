INTRODUCTION
------------

Openclinic core module is a main module Openclinic distribution.
It consists of index page and configuration form.
Index page described openclinic installation process.
Configuration form works with openclinic main settings which get
data from static YAML-files.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable the module.


CONFIGURATION
-------------

* Go to [Your Site]/admin/config/openclinic/config
  or: Administration > Configuration > Open clinic > Open clinic config
  and configure Phases, Relations, Base fields.


MAINTAINERS
-----------

Current maintainers:
 * Sergey Loginov (goodboy) - https://drupal.org/user/222910
