<?php

namespace Drupal\openclinic_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'openclinic.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('openclinic.config');

    $phases = $config->get('phases');
    $phases = $phases ? $this->getYaml($phases) : $this->getYamlFile('phases');
    $form['phases'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Phases'),
      '#description' => $this->t('Phases in YAML format.'),
      '#default_value' => trim($phases),
      '#required' => TRUE,
    ];

    $relations = $config->get('relations');
    $relations = $relations ? $this->getYaml($relations) : $this->getYamlFile('relations');
    $form['relations'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Relations'),
      '#description' => $this->t('Relations in YAML format.'),
      '#default_value' => trim($relations),
      '#required' => TRUE,
    ];

    $base_fields = $config->get('base_fields');
    $base_fields = $base_fields ? $this->getYaml($base_fields) : $this->getYamlFile('base_fields');
    $form['base_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Base fields'),
      '#description' => $this->t('Base fields in YAML format.'),
      '#default_value' => trim($base_fields),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $phases = $form_state->getValue('phases');
    if (!$this->checkYaml($phases)) {
      $form_state->setErrorByName('phases', $this->t('Enter a valid Yaml.'));
    }

    $relations = $form_state->getValue('relations');
    if (!$this->checkYaml($relations)) {
      $form_state->setErrorByName('relations', $this->t('Enter a valid Yaml.'));
    }

    $base_fields = $form_state->getValue('base_fields');
    if (!$this->checkYaml($base_fields)) {
      $form_state->setErrorByName('base_fields', $this->t('Enter a valid Yaml.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $phases = Yaml::parse($form_state->getValue('phases'));
    $relations = Yaml::parse($form_state->getValue('relations'));
    $base_fields = Yaml::parse($form_state->getValue('base_fields'));

    // Save phases, relations, base_fields in JSON format.
    $this->config('openclinic.config')
      ->set('phases', json_encode($phases, TRUE))
      ->set('relations', json_encode($relations, TRUE))
      ->set('base_fields', json_encode($base_fields, TRUE))
      ->save();
  }

  /**
   * Get Yaml file data.
   *
   * @param string $name
   *   Filename.
   *
   * @return string|bool
   *   Get Yaml file content.
   */
  protected function getYamlFile($name) {
    $path = drupal_get_path('module', 'openclinic_core') . '/config/' . $name . '.yml';
    if (!file_exists($path)) {
      $this->messenger()->addMessage('File @path doesn\'t exist', ['@path' => $path], 'error');
      return NULL;
    }

    return file_get_contents($path);
  }

  /**
   * Get Yaml data.
   *
   * @param string $json
   *   JSON data.
   *
   * @return string
   *   Get Yaml as string.
   */
  protected function getYaml($json) {
    $array = json_decode($json, TRUE);
    if (empty($array)) {
      return '';
    }
    return Yaml::dump($array);
  }

  /**
   * Check Yaml file data.
   *
   * @param string $data
   *   File content.
   *
   * @return bool
   *   Is Yaml file correct.
   */
  protected function checkYaml($data) {
    try {
      Yaml::parse($data);
    }
    catch (\Exception $e) {
      return FALSE;
    }

    return TRUE;
  }

}
