<?php

namespace Drupal\openclinic_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Index.
   *
   * @return string
   *   Return Hello string.
   */
  public function index() {
    $text = '<h2>Welcome to Open clinic</h2>';
    $steps = [];

    // Check for configuration set.
    $config = $this->config('openclinic.config');
    $has_configuration = ($config->get('phases')
      && $config->get('relations')
      && $config->get('base_fields')
    );

    if (!$has_configuration) {
      $steps[] = 'Go to the ' . Link::fromTextAndUrl('configuration', Url::fromRoute('openclinic.config'))->toString();
    }
    else {
      $steps[] = 'Configuration complete';
    }

    // Check for openclinic modules enabled.
    $modules = [
      'openclinic_install',
      'openclinic_views',
      'views',
      'block',
      'rest',
      'openclinic_pricefield',
    ];

    $items_modules = [];
    foreach ($modules as $module) {
      $module_exists = $this->moduleHandler()->moduleExists($module);
      $enabled_text = $module_exists ? $this->t('Enabled') : $this->t('Disabled');
      $items_modules[] = [
        '#wrapper_attributes' => [
          'class' => ['item'],
        ],
        '#children' => $module . ': ' . $enabled_text,
      ];
    }

    $render_array_list = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $items_modules,
    ];
    $steps[] = 'Open clinic modules: ' . $this->renderer->render($render_array_list);

    // Go to installation.
    if ($has_configuration && $this->moduleHandler()->moduleExists('openclinic_install')) {
      $steps[] = 'Go to the ' . Link::fromTextAndUrl('installation', Url::fromRoute('openclinic_install.install'))->toString();
    }

    $items = [];
    foreach ($steps as $step) {
      $items[] = [
        '#wrapper_attributes' => [
          'class' => ['item'],
        ],
        '#children' => $step,
      ];
    }
    $render_array_list = [
      '#theme' => 'item_list',
      '#list_type' => 'ol',
      '#items' => $items,
    ];
    $text .= $this->renderer->render($render_array_list);

    return [
      '#type' => 'markup',
      '#markup' => $text,
    ];
  }

}
