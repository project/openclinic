#!/bin/bash

composer update
drush site-install --site-name="Open clinic"
drush en openclinic_core  -y
drush en openclinic_install  -y 
drush en openclinic_pricefield  -y
drush en openclinic_views -y
drush en rest -y
composer require drupal/admin_toolbar
drush en admin_toolbar  -y
@echo Open clinic installed
